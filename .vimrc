"call plug#begin('~/.vim/plugged')

"Plug 'airblade/vim-gitgutter'
"Plug 'anned20/vimsence'
"Plug 'cespare/vim-toml'
"Plug 'doy/vim-autobrace'
"Plug 'tpope/vim-eunuch'
"Plug 'ziglang/zig.vim'

"call plug#end()

" Plugin Settings "
" --------------- "
" netrw (File explorer)
let g:netrw_keepdir=0

" GitGutter
"set signcolumn=yes
"autocmd ColorScheme * highlight! link SignColumn LineNr

" Zig.vim
"let g:zig_fmt_autosave = 0

" Color Scheme     "
" ---------------- "
"colorscheme ron
"colorscheme delek
" To install, clone from
" https://github.com/morhetz/gruvbox.git
" into
" ~/.vim/pack/default/start/gruvbox
autocmd vimenter * ++nested colorscheme gruvbox
set background=dark

" General Settings "
" ---------------- "
set number
syntax on

set autoindent          " Keep Indentation when adding newlines
set smartindent         " Fix indents for {} lines
set tabstop=4           " Number of spaces that a tab counts for
set shiftwidth=0        " Use tabstop instead of shiftwidth for auto-indent
set softtabstop=4       " ?
set expandtab           " Insert spaces instead of tab characters

" De-emphasize bracket matches
highlight MatchParen cterm=underline ctermbg=none ctermfg=none

" Render invisible chars
set list
" Alternate chars to render tabs/spaces
set listchars=tab:>-,space:·

" Hide all space chars
highlight Spaces cterm=none ctermfg=none ctermbg=none
match Spaces / /

" Show any groups of spaces with more than 1 space
"highlight MultipleSpaces ctermfg=0
"2match MultipleSpaces / \{2,}/

highlight SpecialKey ctermfg=1
" Prevent removing whitespace when creating blank lines
inoremap <cr> <space><bs><cr>

" Set netrw tree view on cd
autocmd DirChanged * :execute "Ntree ". getcwd()

" Strip trailing whitespace on nonblank lines on save
autocmd BufWritePre * :call Preserve("%s/\\(\\S\\)\\s\\+$/\\1/e")

" Language specific settings "
" -------------------------- "
filetype plugin on

autocmd BufRead,BufNewFile *.md set textwidth=80

" Finally, open . if there's no file to edit "
" ------------------------------------------ "
autocmd VimEnter * if eval("@%") == "" | edit . | endif

" Misc Functions "
" -------------- "
" Prevent a cmd from moving the cursor or changning search history
function! Preserve(cmd)
    " Save last search, and cursor pos
    let last_search = @/
    let line = line(".")
    let col = col(".")
    
    execute a:cmd
    
    let @/ = last_search
    call cursor(line, col)
endfunction

