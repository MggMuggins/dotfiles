#!/bin/bash

export PATH="$HOME/.local/go/bin:$PATH"

export EDITOR=hx
export GPGKEY=2C207C45

export DEBFULLNAME="Wesley Hershberger"
export DEBMAIL="wesley.hershberger@canonical.com"
export UBUMAIL="wesley.hershberger@canonical.com"
export QUILT_PATCHES=debian/patches
export TZ=America/Chicago

export LEDGER_FILE="$HOME/Documents/accounting/accounts.journal"

alias bat="batcat"
alias py="python3"
alias lx="hx"  # Home-row-ish helix 
alias m8ctl="microk8s kubectl"
alias os="openstack"
alias sdctl="sudo systemctl"

alias sts_admin_net_sshuttle="sudo sshuttle -e \"ssh -i /home/wesley/.ssh/id_ed25519\" -H -r ubuntu@bastion 10.5.0.0/16"
alias watch-juju="juju status --watch 2s"

alias git-ls-objs="git cat-file --batch-check --batch-all-objects"

# Colors
alias ip="ip -c=auto"
alias diff="diff --color=auto"

# Handy
SED_NL_AWK='{gsub(/\\n/,"\n"); gsub(/\\t/, "\t");}1;'
alias sed_nl="awk '$SED_NL_AWK'"

# Support
alias sosgrab="~/.local/src/get-sts-case-files/get-case-files.bash"
alias hotsos="hotsos -s --sos-unpack-dir ./"

hotsos-all() {
    for rpt in *.tar.xz; do
        echo "$rpt"
        hotsos "$rpt" > "${rpt}.hotsos"
    done
}

resolve-lxd() {
    # To enable dns resolution to local lxd instances:
    sudo resolvectl dns lxdbr0 10.183.135.1
    
    # Route .lxd domains to the lxd bridge
    sudo resolvectl domain lxdbr0 lxd
}

lxc-me() {
    if [[ -z "$1" ]]; then
        echo "lxc-me: missing container name"
        return 1
    fi
    lxc exec "$1" -- su --login wesley
}

_lxc-me_completions() {
    COMPREPLY=($(compgen -W "$(lxc ls --columns sn --format compact | awk '$1 == "RUNNING" {print $2}')" "${COMP_WORDS[1]}"))
}

complete -F _lxc-me_completions lxc-me

