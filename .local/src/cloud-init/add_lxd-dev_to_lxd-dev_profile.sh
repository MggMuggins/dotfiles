#!/bin/bash

# `base.ccfg` has my dotfiles and some other stuff.

# This script adds it to the default lxc profile

if [[ -n "$1" ]]; then
  profile="$1"
else
  profile=lxd-dev
fi

user_data="$(cloud-init devel make-mime -a ./base.ccfg:jinja2 -a ./lxd-dev.ccfg:jinja2)"

echo "Adding cloud-init to ${profile}:"
echo "================================"

echo "${user_data}"

lxc profile set "${profile}" user.user-data "${user_data}"

