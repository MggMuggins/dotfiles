#!/bin/bash

# `base.ccfg` has my dotfiles and some other stuff.

# This script adds it to the default lxc profile

if [[ -n "$1" ]]; then
  profile="$1"
else
  profile=default
fi

lxc profile set "$profile" user.user-data "$(cat ./base.ccfg)"

