## template: jinja
#cloud-config

groups:
  - wesley
  - adm
  - systemd-journal
  - netdev
  - lxd
  - landscape
  - ceph
  - libvirt
  - microk8s

users:
  - name: wesley
    gecos: Wesley Hershberger
    primary_group: wesley
    groups:
      - lxd
      - microk8s
      - adm
      - systemd-journal
      - sudo
    lock-passwd: true
    lock_passwd: true
    shell: /bin/bash
    sudo: "ALL=(ALL) NOPASSWD:ALL"
    uid: "1000"
    ssh_import_id:
      - lp:whershberger

package_update: true
package_upgrade: true

snap:
  commands:
    - snap install helix --classic

{% set packages = {
    "kakoune": 16.04,
    "bat": 20.04,
    "tree": 14.04,
    "plocate": 22.04,
    "lnav": 14.04,
    "ripgrep": 20.04,
    "duf": 22.04,
    "tig": 14.04,
} %}

packages: [
{% for pkg_name, first_release in packages.items() %}
    {% if first_release <= v1.distro_version|float %}
        {{ pkg_name }},
    {% endif %}
{% endfor %}
]

timezone: America/Chicago

{% set git_cmds = [
    "git init /home/wesley",
    "git -C /home/wesley remote add origin https://gitlab.com/MggMuggins/dotfiles.git",
    "git -C /home/wesley fetch origin",
    "git -C /home/wesley switch master",
] %}

{% set proxy_vars = "http_proxy=http://squid.internal:3128 https_proxy=http://squid.internal:3128 ftp_proxy=http://squid.internal:3128 " %}

runcmd:
    {% for git_cmd in git_cmds %}
        {% if "cloud.sts" in ds.meta_data.hostname %}
            {% set git_cmd = proxy_vars ~ git_cmd %}
        {% endif %}
        - ["su", "wesley", "-c", {{ git_cmd }}]
    {% endfor %}

