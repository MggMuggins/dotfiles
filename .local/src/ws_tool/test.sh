#!/bin/bash

set -ex

for testcase in ./tests/*; do
  cat "$testcase" | python3 v2.py | diff "$testcase" -
done

echo "All tests pass!"
