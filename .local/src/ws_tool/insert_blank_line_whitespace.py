#!/usr/bin/env python3

from dataclasses import dataclass, field
import sys


# The things I do to allow Windows-style line endings...
# Rather, the things I do to avoid `print()`...
@dataclass(init=False)
class ThreeLineWindowReader:
    prev: bytes
    curr: bytes
    next: bytes
    leftover: bytes = field(default_factory=bytes)

    def __init__(self):
        self.eof = False
        self.leftover = b""
        self.prev = b""
        self.curr = self._pop_next_line()
        self.next = self._pop_next_line()

    def advance(self):
        """Moves the window to the next line

        Returns True if bytes remain to be read, False if eof is reached
        """
        # delay reporting eof until self.next has been totally emptied
        old_eof = self.eof

        self.prev = self.curr
        self.curr = self.next
        self.next = self._pop_next_line()
        return not old_eof

    def _next_break_indx(self):
        try:
            # We want to include the newline in the bytes we return
            return self.leftover.index(b'\n') + 1
        except ValueError:
            # No newline was found
            return None

    def _pop_next_line(self):
        break_at = self._next_break_indx()

        if break_at is None:
            self.leftover += sys.stdin.buffer.read1(4096)
            break_at = self._next_break_indx()

            if break_at is None:
                # No newline was found before eof
                break_at = len(self.leftover)
                self.eof = True

        next_line = self.leftover[:break_at]
        self.leftover = self.leftover[break_at:]

        return next_line


def stdout_bytes(b):
    sys.stdout.buffer.write(b)


def whitespace_of(b):
    # indx will be undefined in this case
    if len(b) == 0:
        return b

    for indx, byte in enumerate(b):
        if byte not in b' \t':
            break
    return b[:indx]


if __name__ == "__main__":
    reader = ThreeLineWindowReader()
    # The first line ends up in curr right after construction
    stdout_bytes(reader.curr)

    if reader.eof:
        stdout_bytes(reader.next)
        sys.exit(0)

    while reader.advance():
        if reader.curr == b'\n':
            prev_ws = whitespace_of(reader.prev)
            next_ws = whitespace_of(reader.next)

            if prev_ws[:len(next_ws)] == next_ws:
                reader.curr = next_ws + b'\n'

        stdout_bytes(reader.curr)

    # the last call to reader.advance() pushes the last line into curr
    stdout_bytes(reader.curr)
