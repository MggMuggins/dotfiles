# Whitespace Management
Or, my rant on trailing whitespace.

Many developers will tell you they are very picky about whitespace. What they
usually mean is that they don't like trailing whitespace: it clutters git
history. This is true, but I like trailing whitespace on blank lines when I'm
editing: it streamlines my process by keeping my cursor where I want it.

Soon after I started programing I started using
[Atom](https://github.com/atom/atom) (RIP). I remember being blown away that
the text editor could show you exactly what whitespace characters were in your
file. Trailing spaces on code lines are gross: I was keen to remove them.
Atom's whitespace settings circa 2015 included an option to strip trailing
whitespace when saving the file. However, it could also keep trailing
whitespace when the line was **only** whitespace.

For example, the trailing spaces here would be removed:
```python
sum = 0  
for x in nums:    
    sum += (x - mean) ** 2      
```

But the trailing spaces here wouldn't be:
```python
def standardize(nums: list):
    (std_dev, mean) = std_deviation(nums)
    
    for i, n in enumerate(nums):
        nums[i] = (n - mean) / std_dev
```

I liked this behavior because keeping the indentation on blank lines meant I
didn't have to re-indent lines when I wanted to add a line of code, and I could
mark out a couple of blank lines where I intended to write something but wasn't
quite sure what it needed to be yet.

I stopped using Atom for a number of reasons, but I've never been able to find
this combination of configuration options anywhere. That's probably a good
thing: comitting whitespace on blank lines makes me look dumb at best and
inconsiderate at worst.

But...

I still want to edit with blank-line whitespace! But when should the whitespace
be removed? The editor could do it on save, but that happens far too often.
Since I'm removing whitspace for the sake of git, why don't I try to make git
do it?

First I tried to set up a pre-commit git hook that would strip the whitespace
from all source code files during the commit process. The problem
with this is any modified files
need to be checked back into the index after having trailing whitespace
removed. But if that file has been modified since it was checked in and I don't
want to commit those unstaged changes, our working directory is no longer safe
to use for stripping whitespace. The complexity blows up with other similar
corner cases.

Git also allows a user to define arbitrary filters for when files are checked
into the index and copied into the working tree. In .gitconfig:
```
[filter "whitespace"]
	clean = sed 's/[ \t]*$//'
	smudge = insert_blank_line_whitespace.py
```

And apply the filter globally to a bunch of file types (see my `.gitattributes`
for all the patterns I have this enabled for):
```
**/*.py	filter=whitespace
```

This attribute removes trailing whitespace from a file when it is checked into
git, and my python script inserts whitespace when a blank line is surrounded by
two lines with the same level of indentation. This way I can edit my files the
way that I like to and I don't commit whitespace and bug anybody else who is
working on the same code.

This does bring to light a problem with git that is not always obvious: it is
possible to create commits whose state has *never existed in the working
directory*. You might commit code that worked in *your* working directory, but
if you forgot to add a file to the commit then that commit is essentially
untested. This is one of the reasons that CI/CD systems exist on Gitlab/Github.

The obvious danger with my gitattributes is that the code I'm committing is not
exactly the same as the code that I've been editing and running in my working
directory. I'm willing to accept this risk:
- The `sed` that does the whitespace stripping is obvious (if not portable)
  and simple enough that it's verifiable.
- If `insert_blank_line_whitespace.py` is broken, I will know: `sed` should
  entirely undo any changes it makes, so if I check out and immediately have
  changes, then something is amiss. The output of the script never makes it
  into the repo without going through my code review and that sed.
- If code matters enough to be concerned about what's in git, then there will
  be a CI/CD system between me and prod.

I haven't been using this system for very long so we'll see if it stands the
test of time. But I'm excited about having this problem solved in a way that
doesn't impact my current workflow.
