#!/usr/bin/env python3

import sys

from gpiozero import DigitalOutputDevice


def send(bytes):
    sys.stdout.buffer.write(bytes)
    sys.stdout.flush()


power_strip = DigitalOutputDevice(17)

while True:
    msg = sys.stdin.buffer.read(1)

    print(f"recv msg: ({msg})")

    if msg is None or len(msg) == 0:
        break
    elif msg == b"o":
        power_strip.on()
        # send(b"o")
    elif msg == b"f":
        power_strip.off()
        # send(b"f")
    else:
        send(bytes(f"Invalid msg: ({msg})"))

sys.exit(1)
