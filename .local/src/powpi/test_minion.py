from sys import stdout
import time


def msg(bytes):
    stdout.buffer.write(bytes)
    stdout.buffer.flush()


msg(b"o")
time.sleep(3)
msg(b"f")
time.sleep(1)
msg(b"o")
time.sleep(3)
msg(b"f")
time.sleep(1)

msg(b"l")
