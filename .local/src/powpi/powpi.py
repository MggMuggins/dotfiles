#!/usr/bin/env python3

# Based on https://gitlab.com/MggMuggins/battnote

import os
import pwd
import subprocess
import sys
import time

import jc.parsers.acpi


PAUSE_DURATION = 60
CONTROLLER_ADDR = "battnote@cassini.lan"
SECRET_KEY_FILE = "/home/wesley/.ssh/powpi_client_ed25519"


def new_conn():
    uid = os.getuid()
    user = pwd.getpwuid(uid).pw_name
    print(f"Connecting to {CONTROLLER_ADDR} from user {user}(uid={uid})", file=sys.stderr)

    try:
        os.stat(SECRET_KEY_FILE)
    except FileNotFoundError:
        print(f"Secret key file ({SECRET_KEY_FILE}) not found", file=sys.stderr)
        os.exit(1)

    return subprocess.Popen(
        ["ssh", CONTROLLER_ADDR, "/usr/local/bin/powpi_minion.py",
            "-i", SECRET_KEY_FILE, "-vvv"],
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
    )


# Give a little time before we try and open up the network after login
# Possibly prevent a race condition?
time.sleep(3)
CONNECTION = new_conn()


def check_conn():
    global CONNECTION

    CONNECTION.poll()

    while CONNECTION.returncode is not None:
        print(f"Connection dropped with code ({CONNECTION.returncode})", file=sys.stderr)
        print("stdout:", CONNECTION.stdout.read(), file=sys.stderr)
        print("stderr:", CONNECTION.stderr.read(), file=sys.stderr)

        print(f"Retrying in {PAUSE_DURATION} seconds...", file=sys.stderr)
        time.sleep(PAUSE_DURATION)
        CONNECTION = new_conn()


def send(bytes):
    print(f"Send: ({bytes})")
    CONNECTION.stdin.write(bytes)
    CONNECTION.stdin.flush()


batpcnt = 65

while True:
    check_conn()

    acpi_out = subprocess.run(["acpi", "-b"], capture_output=True, text=True)
    try:
        # Workaround known jc bug #356
        acpi_out = jc.parsers.acpi.parse(acpi_out.stdout)
        batpcnt = acpi_out[0]["charge_percent"]
    except AttributeError:
        print(f"jc parse of ACPI failed", file=sys.stderr)

    power_online_file = open("/sys/class/power_supply/AC/online", "r")
    power_online = power_online_file.read().strip() == "1"

    if batpcnt == 0:
        print("Invalid battery percentage", file=sys.stderr)
    elif batpcnt >= 70 and power_online:
        send(b"f")
    elif batpcnt <= 65 and not power_online:
        send(b"o")

    time.sleep(PAUSE_DURATION)
